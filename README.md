# README #

Repositório criado para subida do fonte que visa disponibilizar um serviço de consulta de cep.

Esse README detalha quais as tecnologias necessárias para subir a aplicação em um ambiente de desenvolvimento.

### Estratégia ###
A escolha das tecnologias foi baseada em minha experiência de mercado e também baseado nos requisitos dos enunciados.
Então uma das principais soluções foi um uso de JAVAEE7 que contém algumas soluções que atendem aos requisitos, por exemplo os requisitos:

- Obrigatório receber e retornar JSON :
  Para trabalhar com JSON a especificação de java disponibiliza uma maneira fácil com REST presente desde a JAVAEE6, pois facilita o modo de como disponibilizar uma api rest retornando JSON e como manipular    objetos de retorno sem muita dor de cabeça.

- Usar algum banco ou banco em memória:
  O Java possui um banco chamado H2 e que facilmente integrado ao servidor de aplicação Wildfly. Com ele consegui manter os dados de endereço em memória e realizar todos os testes da API. E para a persistência utilizei JPA para facilitar o trabalho de conexão e manipulação dos dados com a base.



### Tecnologias utilizadas ###

* JAVA 8
* JAVAEE 7
* REST API
* JPA
* Servidor Wildfly 10
* Banco de Dados H2 em memória para salvar os dados do endereço do serviço CRUD

### Documentação e teste ###
Para realizar a documentação do fonte e serviços, foram criados os testes nos pacotes ejb e web. 

Utilizei as seguintes apis JAVA para testes:

* JUNIT 

* MOCKITO para utilizar os recursos de Mock e Injeção

Para verificar se os testes unitários estão passando, basta rodar o comando maven (mvn test) no diretorio raiz do projeto 

### Serviço de validação do Caracter único no STREAM ###
Para solucionar o exercício utilizei a seguinte estratégia:
 * No momento da iteração do stream utilizei Collection Set para garantir a unicidade do carácter na collection

 * Após a inserçao na collection Set o metodo retorna um Boolean TRUE para inserido com sucesso e FALSE para registro não inserido

 * Com isso usei esse retorno para ir adicionando em um List que diferente de Set me garante a ordenção dos dados de entrada

 * Então após o fim da iteração terei a certeza que o primeiro dado da lista é único

 * Então peguei a lista final e retorno o dado de index zero

 A solução encontra-se no pacote br.com.character do project-ejb. Para acessar o método para teste, acesse a classe: StreamService.

 Criei alguns cenários no JUNIT para testar a classe e está no pacote de testes do project-ejb.


### Para consumir o serviço ###
* Rodar os comandos na raiz do projeto: mvn clean install
* Copiar o arquivo project-ear.ear e inserir na pasta standalone/deployments do Wildfly
* Subir o Wildfly na porta 8080
* Utilizar uma ferramente para consumir o serviço REST (Utilize Postman).

### Consumindo as APIS REST ###
Após subir o servidor todas as APIS estarão no contexto (http://localhost:8080/api)


### Consumindo serviço de CEP ###

Segue abaixo uma descrição de como consumir os serviços de busca de endereço e validação de CEP:

GET No contexto /postalcode/{postalcode}

Altere o valor da url {postalcode} para o cep desejado

Retorno status 200 para sucesso na busca e validação + JSON de resposta com o seguinte formato:

![cewpp.PNG](https://bitbucket.org/repo/LM6AA6/images/1639587205-cewpp.PNG)

Retorno status 400 para cep inválido + objeto JSON com o seguinte formato:

![Capasasturar.PNG](https://bitbucket.org/repo/LM6AA6/images/270060137-Capasasturar.PNG)


### Consumindo o serviço de Endereços ###

Segue abaixo uma descrição de como consumir os serviços do Crud de endereços:


* ### NOVO ENDEREÇO ###
   PUT no contexto "/address"

   Entrada Inserir JSON no entity para consumir o serviço

   Retorna o JSON com os dados inseridos + ID gerado na base exemplo:

![1.PNG](https://bitbucket.org/repo/LM6AA6/images/456753880-1.PNG)

* Exemplo de JSON para consumir o serviço:

![Capturar.PNG](https://bitbucket.org/repo/LM6AA6/images/379808851-Capturar.PNG)


* ### ATUALIZAR ENDEREÇO ###
   
   POST no contexto "/address/{addressId}"

   Onde {addressId} é o ID do endereço que deseja alterar

   Entrada Inserir JSON no entity para consumir o serviço

   Retorno status 200 para sucesso na atualização do endereço

   Retorno status 400 para erro na atualização do endereço + JSON de resposta com o seguinte formato:

![Capturar2.PNG](https://bitbucket.org/repo/LM6AA6/images/786524368-Capturar2.PNG)

* Exemplo de JSON para consumir o serviço: 

![33.PNG](https://bitbucket.org/repo/LM6AA6/images/1133758721-33.PNG)


* ### REMOVER ENDEREÇO ###

   DELETE no contexto "/address/{addressId}"

   Onde {addressId} é o ID do endereço que deseja remover

   Retorno status 200 para sucesso na remoção do endereço

   Retorno status 400 para erro na remoção do endereço


* ### PESQUISAR ENDEREÇO POR ID ###
  
  GET no contexto "/address/{addressId}"
  
  Onde {addressId} é o ID do endereço que deseja pesquisar

  Retorno status 200 para sucesso na busca + JSON de resposta com o seguinte formato

![1.PNG](https://bitbucket.org/repo/LM6AA6/images/1442299506-1.PNG)

  Retorno status 204 para busca sem retorno