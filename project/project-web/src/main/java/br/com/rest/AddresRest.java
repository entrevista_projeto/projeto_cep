package br.com.rest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.entity.AddresEntity;
import br.com.rest.to.MessageResponse;
import br.com.service.AddressService;
import br.com.service.exception.AddressException;
import br.com.util.Constantes;
import br.com.util.messages.MessageUtil;

@Path("/address")
@RequestScoped
public class AddresRest {

	@EJB
	private AddressService addressService;
	
	public AddresRest() {
		super();
	}
	
	public AddresRest(AddressService addressService) {
		this.addressService = addressService;
	}

	@GET
	@Path("/{addressId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response read(@PathParam("addressId") Integer addressId) {

		AddresEntity addresEntity = null;
		try {
			addresEntity = addressService.searchById(addressId);
		} catch (AddressException e) {
			return Response.status(Status.NO_CONTENT).build();
		}

		return Response.status(Status.OK).entity(addresEntity).build();
	}

	@DELETE
	@Path("/{addressId}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response delete(@PathParam("addressId") Integer addressId) {

		try {
			addressService.remove(addressId);
		} catch (AddressException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessages()).build();
		}
		MessageResponse msg = new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_REMOVIDO_SUCESSO));
		return Response.status(Status.OK).entity(msg).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response create(AddresEntity address) {
		try {
			addressService.save(address);
		} catch (AddressException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessages()).build();
		}

		return Response.status(Status.OK).entity(address).build();
	}

	@POST
	@Path("/{addressId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response update(@PathParam("addressId") Integer addressId, AddresEntity address) {
		try {
			address.setId(addressId);
			addressService.update(address);
		} catch (AddressException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessages()).build();
		}
		return Response.status(Status.OK).entity(address).build();
	}

}
