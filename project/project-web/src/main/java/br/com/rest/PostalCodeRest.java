package br.com.rest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.entity.AddresEntity;
import br.com.rest.to.PostalCodeTo;
import br.com.service.PostalCodeService;
import br.com.service.exception.PostalCodeException;

@Path("/postalcode")
@RequestScoped
public class PostalCodeRest {

	@EJB
	private PostalCodeService postalCodeService;
	
	public PostalCodeRest(PostalCodeService postalCodeService) {
		this.postalCodeService = postalCodeService;
	}

	public PostalCodeRest() {
		super();
	}
	
	@GET
	@Path("/{postalCode}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response search(@PathParam("postalCode") String postalCode) {
		PostalCodeTo postalCodeTo = null;
		try {			
			AddresEntity postalCodeEntity = postalCodeService.search(postalCode);
			postalCodeTo = new PostalCodeTo(postalCodeEntity);
		} catch (PostalCodeException e) {			
			return Response.status(Status.BAD_REQUEST).entity(e.getMessages()).build();
		}
		
		return Response.status(Status.OK).entity(postalCodeTo).build();
	}
	
	public Response searchtest(@PathParam("postalCode") String postalCode) {
		PostalCodeTo postalCodeTo = null;
		try {			
			AddresEntity postalCodeEntity = postalCodeService.search(postalCode);
			postalCodeTo = new PostalCodeTo(postalCodeEntity);
		} catch (PostalCodeException e) {			
			return Response.status(Status.BAD_REQUEST).entity(e.getMessages()).build();
		}
		
		return Response.status(Status.OK).entity(postalCodeTo).build();
	}

}
