package br.com.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath ( "/api" )
public class RestConfig extends Application {

}