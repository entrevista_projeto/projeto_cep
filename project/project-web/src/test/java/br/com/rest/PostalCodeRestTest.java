package br.com.rest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.entity.AddresEntity;
import br.com.rest.to.MessageResponse;
import br.com.service.PostalCodeService;
import br.com.service.exception.PostalCodeException;
import br.com.util.Constantes;
import br.com.util.messages.MessageUtil;

public class PostalCodeRestTest {

	private PostalCodeRest rest;
	
	@Mock
	private PostalCodeService postalCodeService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		rest = new PostalCodeRest(postalCodeService);
	}

	@Test
	public void postalCodeInvalid() throws PostalCodeException {
		String postalCoDeInvalid = "1234567892365889";
		
		List<MessageResponse> erros = new ArrayList<>();
		erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_CEP_INVALIDO)));
		when(postalCodeService.search(postalCoDeInvalid)).thenThrow(new PostalCodeException(erros));
		
		Response response = rest.search(postalCoDeInvalid);
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
		
	}

	@Test
	public void postalCodeNull() throws PostalCodeException {
		String postalCoDeInvalid = null;
		List<MessageResponse> erros = new ArrayList<>();
		erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_CEP_INVALIDO)));
		when(postalCodeService.search(postalCoDeInvalid)).thenThrow(new PostalCodeException(erros));
		
		Response response = rest.search(postalCoDeInvalid);
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void postalCodeEmpty() throws PostalCodeException {
		String postalCoDeInvalid = "";
		List<MessageResponse> erros = new ArrayList<>();
		erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_CEP_INVALIDO)));
		when(postalCodeService.search(postalCoDeInvalid)).thenThrow(new PostalCodeException(erros));
		
		Response response = rest.search(postalCoDeInvalid);
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void postalCodeValid() throws PostalCodeException {
		String postalCoDeInvalid = "09861690";
		AddresEntity address = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		
		when(postalCodeService.search(postalCoDeInvalid)).thenReturn(address);		
		Response response = rest.search(postalCoDeInvalid);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}


	@Test
	public void postalCodeReturnAddressCharacter() throws PostalCodeException {
		String postalCoDeInvalid = "09861-690";
		AddresEntity address = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		
		when(postalCodeService.search(postalCoDeInvalid)).thenReturn(address);		
		Response response = rest.search(postalCoDeInvalid);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}

}
