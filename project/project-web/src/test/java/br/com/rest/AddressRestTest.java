package br.com.rest;

import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.dao.address.AddressDao;
import br.com.entity.AddresEntity;
import br.com.service.AddressService;
import br.com.service.PostalCodeService;
import br.com.service.exception.AddressException;


public class AddressRestTest {
	
	private AddresRest rest;

	@Mock
	private AddressDao dao;

	@Mock
	private AddressService addressService;
	
	@Mock
	private PostalCodeService postalCodeService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		rest = new AddresRest(addressService);
	}

	@Test
	public void search() throws AddressException {
		AddresEntity addressSave = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		addressSave.setId(1);

		when(addressService.searchById(addressSave.getId())).thenReturn(addressSave);		
		Response response = rest.read(addressSave.getId());
		
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void searchNoResult() throws AddressException {
		AddresEntity addressSave = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		addressSave.setId(1);

		when(addressService.searchById(addressSave.getId())).thenThrow(new AddressException());		
		Response response = rest.read(addressSave.getId());
		
		Assert.assertEquals(Status.NO_CONTENT.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void createSuccess() throws AddressException {
		AddresEntity address = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");

		AddresEntity addressSave = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		addressSave.setId(1);

		when(addressService.save(address)).thenReturn(addressSave);		
		Response response = rest.create(address);
		
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void createInvalideStateCity() throws AddressException {
		AddresEntity address = new AddresEntity("rua Vicente", "1000", null, "09896200", null, "B Assunção",
				"AP 23");


		when(addressService.save(address)).thenThrow(new AddressException());		
		Response response = rest.create(address);
		
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void updateSuccess() throws AddressException {

		AddresEntity addressSave = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		addressSave.setId(1);

		when(addressService.update(addressSave)).thenReturn(addressSave);		
		Response response = rest.update(addressSave.getId(), addressSave);
		
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	} 
	
	@Test
	public void deleteSuccess() throws AddressException {
		AddresEntity addressSave = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		addressSave.setId(1);

		when(addressService.remove(addressSave.getId())).thenReturn(addressSave);		
		Response response = rest.delete(addressSave.getId());
		
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	} 
	
}
