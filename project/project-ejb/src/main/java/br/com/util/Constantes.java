package br.com.util;

public class Constantes {

	public static final String MSG_CEP_INVALIDO = "msg.cep.invalido";
	public static final String MSG_ENDERECO_INVALIDO = "msg.address.invalido";
	public static final String MSG_RUA_OBRIGATORIO = "msg.address.route.obrigatorio";
	public static final String MSG_NUMERO_OBRIGATORIO = "msg.address.number.obrigatorio";
	public static final String MSG_CEP_OBRIGATORIO = "msg.address.postalcode.obrigatorio";
	public static final String MSG_CIDADE_OBRIGATORIO = "msg.address.city.obrigatorio";
	public static final String MSG_ESTADO_OBRIGATORIO = "msg.address.state.obrigatorio";
	public static final String MSG_ENDEROCO_NAO_EXISTE = "msg.address.not.found";
	public static final String MSG_ENDERECO_ATUALIZADO_SUCESSO = "msg.address.update";
	public static final String MSG_ENDERECO_SALVO_SUCESSO = "msg.address.save";
	public static final String MSG_ENDERECO_REMOVIDO_SUCESSO = "msg.address.remove";
	public static final String MSG_ENDERECO_ERRO_REMOVER = "msg.address.erro.remove";
	public static final String MSG_ENDERECO_ERRO_ALTERAR = "msg.address.erro.update";
	public static final String MSG_ENDERECO_ERRO_SALVAR = "msg.address.erro.insert";

	public static final String LABEL_RUA = "label.route";
	public static final String LABEL_NUMERO = "label.number";
	public static final String LABEL_CIDADE = "label.city";
	public static final String LABEL_ESTADO = "label.state";
	public static final String LABEL_CEP = "label.postalCode";
}
