package br.com.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class HttpUtil {
	private static final Logger logger = Logger.getLogger(HttpUtil.class);
	private static HttpUtil instance = null;
	private String username;
	private String password;
	private static String authBase;

	protected HttpUtil(String username, String password) {
		this.username = username;
		this.password = password;
		String auth = this.username + ":" + this.password;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
		authBase = "Basic " + new String(encodedAuth);
	}

	public HttpUtil() {
		super();
	}

	public static HttpUtil getInstance() {
		if (instance == null) {
			instance = new HttpUtil();
		}
		return instance;
	}

	public static void init(String username, String password) {
		instance = new HttpUtil(username, password);
	}
	
	public static String sendGet(String url) throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");
		
		Properties systemProperties = System.getProperties();
		systemProperties.setProperty("http.proxyHost", "proxy01.prodesp.sp.gov.br");
		systemProperties.setProperty("http.proxyPort", "80");
		
		
		con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	
	}

	public static String get(String url) throws Exception, HttpException {
		String data = null;
		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = null;

		HttpGet request = new HttpGet(url);
		if (authBase != null)
			request.setHeader(HttpHeaders.AUTHORIZATION, authBase);
		try {

			response = client.execute(request);
			data = verificarStatusDoResponse(data, response);

		} catch (ClientProtocolException e) {
			logger.error(e);
			throw new Exception();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e);
			throw new Exception();
		}  finally {
			try {
				if (response != null)
					response.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (client != null)
					client.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	public String post(String url, String params) throws HttpException, Exception {
		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		String data = null;

		try {
			HttpPost httpPost = new HttpPost(url);
			if (params != null) {
				httpPost.setEntity(new StringEntity(params));
				httpPost.setHeader("Content-type", "application/json");
			}

			if (authBase != null)
				httpPost.setHeader(HttpHeaders.AUTHORIZATION, authBase);

			response = client.execute(httpPost);
			data = verificarStatusDoResponse(data, response);

		} catch (ClientProtocolException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		} finally {
			try {
				if (response != null)
					response.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (client != null)
					client.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	public String delete(String url) throws HttpException, Exception {

		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		HttpDelete httpDelete = new HttpDelete(url);
		String data = null;
		if (authBase != null)
			httpDelete.setHeader(HttpHeaders.AUTHORIZATION, authBase);

		try {
			response = client.execute(httpDelete);
			data = verificarStatusDoResponse(data, response);

		} catch (ClientProtocolException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		} finally {
			try {
				if (response != null)
					response.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (client != null)
					client.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	public String put(String url, String params) throws HttpException, Exception {
		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		HttpPut httpPut = new HttpPut(url);
		httpPut.setEntity(new StringEntity(params, "UTF-8"));
		String data = null;
		if (authBase != null)
			httpPut.setHeader(HttpHeaders.AUTHORIZATION, authBase);

		try {
			response = client.execute(httpPut);

			data = verificarStatusDoResponse(data, response);
		} catch (ClientProtocolException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		} finally {
			try {
				if (response != null)
					response.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (client != null)
					client.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	private static String verificarStatusDoResponse(String data, CloseableHttpResponse response)
			throws IOException, HttpException, Exception {
		if (response != null && (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()
				|| HttpStatus.SC_ACCEPTED == response.getStatusLine().getStatusCode())) {
			data = EntityUtils.toString(response.getEntity());
		} else if (HttpStatus.SC_CONFLICT == response.getStatusLine().getStatusCode()) {
			throw new HttpException(response.getStatusLine().getStatusCode(),
					EntityUtils.toString(response.getEntity()));
		} else if (HttpStatus.SC_BAD_REQUEST == response.getStatusLine().getStatusCode()) {
			throw new HttpException(response.getStatusLine().getStatusCode());
		} else if (HttpStatus.SC_UNAUTHORIZED == response.getStatusLine().getStatusCode()) {
			throw new HttpException(response.getStatusLine().getStatusCode(),
					EntityUtils.toString(response.getEntity()));
		} else if (HttpStatus.SC_BAD_GATEWAY == response.getStatusLine().getStatusCode()) {
			throw new Exception();
		} else if (HttpStatus.SC_FORBIDDEN == response.getStatusLine().getStatusCode()) {
			throw new Exception();
		} else {
			throw new Exception();
		}
		return data;
	}

	public String getUsername() {
		return this.username;
	}

}
