package br.com.util;
public class HttpException extends Exception {

	private static final long serialVersionUID = -1939240728989728113L;
	private int status;

	private String message;

	public HttpException() {
		super();
	}

	public HttpException(int status) {
		super();
		this.status = status;
	}

	public HttpException(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
}