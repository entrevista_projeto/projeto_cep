package br.com.util.messages;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageUtil {

	private static Locale locale = Locale.getDefault();

	public static String key(final String key) {
		final String baseName = "messages";
		return ResourceBundle.getBundle(baseName, locale).getString(key);
	}

	public static void setLocale(final String language) {
		locale = new Locale(language);
	}

}
