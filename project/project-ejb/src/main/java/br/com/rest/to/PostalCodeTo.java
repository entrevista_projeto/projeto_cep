package br.com.rest.to;

import br.com.entity.AddresEntity;

public class PostalCodeTo {

	private String rua;

	private String cidade;

	private String estado;

	private String bairro;

	public PostalCodeTo(AddresEntity ad) {
		super();
		this.rua = ad.getRoute();
		this.cidade = ad.getCity();
		this.estado = ad.getState();
		this.bairro = ad.getNeighborhood();
	}

	public String getRua() {
		return rua;
	}

	public String getCidade() {
		return cidade;
	}

	public String getEstado() {
		return estado;
	}

	public String getBairro() {
		return bairro;
	}

}
