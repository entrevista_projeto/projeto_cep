package br.com.rest.to;

import java.util.List;

public class ResponseTo {

	public String error;

	private List<MessageResponse> messages;

	public Object result;

	public ResponseTo() {

	}

	public ResponseTo(String error, List<MessageResponse> messages) {
		super();
		this.error = error;
		this.messages = messages;
	}

	public ResponseTo(Object result) {
		super();

		this.result = result;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public List<MessageResponse> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageResponse> messages) {
		this.messages = messages;
	}

}