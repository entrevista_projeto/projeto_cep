package br.com.rest.to;

public class MessageResponse {

	private String msg;

	public MessageResponse(String msg) {
		super();
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
