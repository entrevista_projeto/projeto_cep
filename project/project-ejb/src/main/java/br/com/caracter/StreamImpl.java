package br.com.caracter;

import java.io.IOException;

public class StreamImpl implements Stream {

	private static final String CONEXÃO_FECHADA_IMPOSSÍVEL_REALIZAR_A_LEITURA = "Conexão fechada. Impossível realizar a leitura.";
	private int index = 0;
	private char[] stream;
	private boolean close;

	public StreamImpl(String stream) {
		if (stream != null && stream.length() > 0)
			this.stream = stream.toCharArray();
	}

	@Override
	public char getNext() throws IOException {
		if (close)
			throw new IOException(CONEXÃO_FECHADA_IMPOSSÍVEL_REALIZAR_A_LEITURA);

		char c = stream[index++];

		if (index == stream.length)
			close = true;

		return c;
	}

	@Override
	public boolean hasNext() {
		return index < stream.length;
	}

}
