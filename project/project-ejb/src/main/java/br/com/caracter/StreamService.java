package br.com.caracter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StreamService {

	/**
	 * Leitura de um stream e retorna o caracter unico
	 * Utilizado Set para garantir a unicidade do dado e List para garantir a ordenação
	 * @param stream
	 * @return
	 * @throws Throwable
	 * @throws IOException
	 */
	public static char firstChar(Stream stream) throws Throwable, IOException {
		Set<Character> setCharacter = new HashSet<>();
		List<Character> charUnicos = new ArrayList<>();

		while (stream.hasNext()) {
			Character c = new Character(stream.getNext());
			if (setCharacter.add(c)) {
				charUnicos.add(c);
			} else {
				charUnicos.remove(c);
			}
		}
		
		if (charUnicos.isEmpty())
			throw new Throwable("Não ha caracteres únicos");

		return charUnicos.get(0);
	}

}
