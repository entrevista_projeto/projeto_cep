package br.com.caracter;

import java.io.IOException;

public interface Stream {
	
	public char getNext() throws IOException;
	public boolean hasNext() ;
}
