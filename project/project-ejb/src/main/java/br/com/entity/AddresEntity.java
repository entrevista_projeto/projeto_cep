package br.com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_address", uniqueConstraints = @UniqueConstraint(columnNames = "id") )
public class AddresEntity {

	@Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "route", nullable = false)
	private String route;
	
	@Column(name = "number", nullable = false)
	private String number;		

	@Column(name = "city", nullable = false)
	private String city;

	@Column(name = "postal_code", nullable = false)
	private String postalCode;

	@Column(name = "state", nullable = false)
	private String state;
	
	@Column(name = "neighborhood")
	private String neighborhood;
	
	@Column(name = "complement")
	private String complement;

	public AddresEntity(String route, String neighborhood, String city, String state) {
		super();
		this.route = route;
		this.neighborhood = neighborhood;
		this.city = city;
		this.state = state;
	}
	

	public AddresEntity(String route, String number, String city, String postalCode, String state, String neighborhood,
			String complement) {
		super();
		this.route = route;
		this.number = number;
		this.city = city;
		this.postalCode = postalCode;
		this.state = state;
		this.neighborhood = neighborhood;
		this.complement = complement;
	}



	public AddresEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
}
