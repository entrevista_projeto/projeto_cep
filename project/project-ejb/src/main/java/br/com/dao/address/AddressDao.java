package br.com.dao.address;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.entity.AddresEntity;
import br.com.rest.to.MessageResponse;
import br.com.service.exception.AddressException;
import br.com.util.Constantes;
import br.com.util.messages.MessageUtil;

@Default
public class AddressDao {

	@PersistenceContext(unitName = "primary")
    private EntityManager em;
	
	private List<AddresEntity> cepsSaoPaulo = null;
	private List<AddresEntity> cepsRioDeJaneiro = null;
	
	public AddressDao(EntityManager em) {
		this.em = em;
	}
	
	public AddressDao() {
		cepsSaoPaulo = new ArrayList<>();
		AddresEntity cep = new AddresEntity("Av. Augusta", "Cerqueira César", "São Paulo", "SP");
		AddresEntity cep1 = new AddresEntity("Av. Paulista", "Jardins", "São Paulo", "SP");
		AddresEntity cep2 = new AddresEntity("Av. Kennedy", "Jardim do Mar", "São Bernardo do Campo", "SP");
		cepsSaoPaulo.add(cep);
		cepsSaoPaulo.add(cep1);
		cepsSaoPaulo.add(cep2);

		cepsRioDeJaneiro = new ArrayList<>();
		AddresEntity cep3 = new AddresEntity("Av. do Mar", "Cerqueira César", "Rio de Janeiro", "RJ");
		AddresEntity cep4 = new AddresEntity("Av. Brasil", "Barra da Tijuca", "Rio de Janeiro", "RJ");
		AddresEntity cep5 = new AddresEntity("Av. João Paulo", "Barra da Tijuca", "São Bernardo do Campo", "SP");
		cepsRioDeJaneiro.add(cep3);
		cepsRioDeJaneiro.add(cep4);
		cepsRioDeJaneiro.add(cep5);
	}

	/**
	 * Não utilizei StrinBuilder porque a operação é pequena demais para
	 * utiliza-la
	 * 
	 * @param args
	 */
	public AddresEntity searchForPostalCode(String postalCode) {
		String pc = postalCode.substring(5, 8);
		int postal = Integer.parseInt(pc);
		AddresEntity cepEntity = null;

		if (postal >= 0 && postal <= 200) {
			int index = new Random().nextInt(cepsSaoPaulo.size());
			cepEntity = cepsSaoPaulo.get(index);
			
		} else if (postal > 200 && postal <= 289) {
			int index = new Random().nextInt(cepsRioDeJaneiro.size());
			cepEntity = cepsRioDeJaneiro.get(index);
		}

		return cepEntity;
	}
	
	
	public AddresEntity save(AddresEntity addresEntity) throws AddressException {	
		try {
			em.persist(addresEntity);			
		} catch (Exception e) {
			List<MessageResponse> erros = new ArrayList<>();
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_ERRO_SALVAR)));
			throw new AddressException(erros);
		}
		return addresEntity;
	}
	
	public AddresEntity update(AddresEntity addresEntity) throws AddressException {
		try {
			
			if (addresEntity != null && addresEntity.getId() != null) {				
				AddresEntity addresExists = em.find(AddresEntity.class, addresEntity.getId());
				if (addresExists != null){
					em.merge(addresEntity);
				}				
			}
						
		} catch (Exception e) {
			List<MessageResponse> erros = new ArrayList<>();
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_ERRO_ALTERAR)));
			throw new AddressException(erros);
		}
		return addresEntity;
	}
	
	public AddresEntity remove(Integer id) throws AddressException {
		AddresEntity addresExists = null;
		try {			
			if (id != null) {				
				addresExists = em.find(AddresEntity.class, id);
				em.remove(addresExists);
			}						
		} catch (Exception e) {
			List<MessageResponse> erros = new ArrayList<>();
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_ERRO_REMOVER)));
			throw new AddressException(erros);
		}
		return addresExists;
	}
	
	public AddresEntity searchById(Integer id) throws AddressException {
		AddresEntity addresExists = null;
		try {			
			if (id != null) {				
				addresExists = em.find(AddresEntity.class, id);	
				if (addresExists == null) {
					List<MessageResponse> erros = new ArrayList<>();
					erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDEROCO_NAO_EXISTE)));
					throw new AddressException(erros);
				}
			}
						
		} catch (NoResultException e) {
			List<MessageResponse> erros = new ArrayList<>();
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDEROCO_NAO_EXISTE)));
			throw new AddressException(erros);
		}
		return addresExists;
	}
}
