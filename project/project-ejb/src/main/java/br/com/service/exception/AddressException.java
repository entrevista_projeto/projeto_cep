package br.com.service.exception;

import java.util.List;

import br.com.rest.to.MessageResponse;

public class AddressException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2359431905549542114L;
	
	private String message;
	
	private List<MessageResponse> messages;
	
	public AddressException(List<MessageResponse> msgs) {
		this.messages = msgs;
	}

	public List<MessageResponse> getMessages() {
		return messages;
	}
	
	public AddressException() {
		super();
	}
	
	public AddressException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}	
	
	
}
