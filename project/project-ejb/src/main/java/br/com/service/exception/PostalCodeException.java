package br.com.service.exception;

import java.util.List;

import br.com.rest.to.MessageResponse;

public class PostalCodeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2359431905549542114L;

	private String message;

	public PostalCodeException() {
		super();
	}

	private List<MessageResponse> messages;

	public PostalCodeException(List<MessageResponse> msgs) {
		this.messages = msgs;
	}

	public List<MessageResponse> getMessages() {
		return messages;
	}

	public PostalCodeException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
