package br.com.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.New;
import javax.inject.Inject;

import br.com.dao.address.AddressDao;
import br.com.entity.AddresEntity;
import br.com.rest.to.MessageResponse;
import br.com.service.exception.AddressException;
import br.com.service.exception.PostalCodeException;
import br.com.util.Constantes;
import br.com.util.messages.MessageUtil;

@Stateless
public class AddressService {

	@Inject
	private PostalCodeService postalCodeService;

	@Inject
	@New
	private AddressDao addressDao;

	public AddressService() {
		super();
	}

	public AddresEntity save(AddresEntity addresEntity) throws AddressException {
		validationSave(addresEntity);
		return addressDao.save(addresEntity);
	}

	public AddresEntity update(AddresEntity addresEntity) throws AddressException {
		validationUpdate(addresEntity);
		return addressDao.update(addresEntity);
	}

	public AddresEntity remove(Integer id) throws AddressException {
		return addressDao.remove(id);
	}

	public AddresEntity searchById(Integer id) throws AddressException {
		return addressDao.searchById(id);

	}

	/**
	 * Realiza as validações dos dados obrigatórios da entidade
	 * 
	 * @param addresEntity
	 * @throws AddressException
	 */
	private void validationSave(AddresEntity addresEntity) throws AddressException {
		boolean valide = true;
		List<MessageResponse> erros = new ArrayList<>();

		if (addresEntity == null) {
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_INVALIDO)));
			throw new AddressException(erros);
		}

		if (addresEntity.getId() != null) {
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_INVALIDO)));
			throw new AddressException(erros);
		}

		valide = validationGeneral(addresEntity, valide, erros);

		if (!valide) {
			throw new AddressException(erros);
		}
	}

	/**
	 * Realiza as validações de alterações na entidade
	 * 
	 * @param addresEntity
	 * @throws AddressException
	 */
	private void validationUpdate(AddresEntity addresEntity) throws AddressException {
		boolean valide = true;
		List<MessageResponse> erros = new ArrayList<>();

		if (addresEntity == null) {
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_INVALIDO)));
			throw new AddressException(erros);
		}

		if (addresEntity.getId() == null) {
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_ENDERECO_INVALIDO)));
			throw new AddressException(erros);
		}

		valide = validationGeneral(addresEntity, valide, erros);

		if (!valide) {
			throw new AddressException(erros);
		}
	}

	private boolean validationGeneral(AddresEntity addresEntity, boolean valide, List<MessageResponse> erros) {
		if (addresEntity.getRoute() == null || addresEntity.getRoute().isEmpty()) {
			erros.add(new MessageResponse(
					MessageUtil.key(Constantes.LABEL_RUA) + ": " + MessageUtil.key(Constantes.MSG_RUA_OBRIGATORIO)));
			valide = false;
		}

		if (addresEntity.getNumber() == null || addresEntity.getNumber().isEmpty()) {
			erros.add(new MessageResponse(MessageUtil.key(Constantes.LABEL_NUMERO) + ": "
					+ MessageUtil.key(Constantes.MSG_NUMERO_OBRIGATORIO)));
			valide = false;
		}

		if (addresEntity.getState() == null || addresEntity.getState().isEmpty()) {
			erros.add(new MessageResponse(MessageUtil.key(Constantes.LABEL_ESTADO) + ": "
					+ MessageUtil.key(Constantes.MSG_ESTADO_OBRIGATORIO)));
			valide = false;
		}

		if (addresEntity.getCity() == null || addresEntity.getCity().isEmpty()) {
			erros.add(new MessageResponse(MessageUtil.key(Constantes.LABEL_CIDADE) + ": "
					+ MessageUtil.key(Constantes.MSG_CIDADE_OBRIGATORIO)));
			valide = false;
		}

		if (addresEntity.getPostalCode() == null || addresEntity.getPostalCode().isEmpty()) {
			erros.add(new MessageResponse(
					MessageUtil.key(Constantes.LABEL_CEP) + ": " + MessageUtil.key(Constantes.MSG_CEP_OBRIGATORIO)));
			valide = false;
		} else {
			try {
				postalCodeService.validPostalCode(addresEntity.getPostalCode());
			} catch (PostalCodeException e) {
				erros.add(new MessageResponse(MessageUtil.key(Constantes.LABEL_CEP) + ": " + e.getMessage()));
				valide = false;
			}
		}
		return valide;
	}

	public AddressService(AddressDao addressDao, PostalCodeService postalCodeService) {
		this.addressDao = addressDao;
		this.postalCodeService = postalCodeService;
	}
}
