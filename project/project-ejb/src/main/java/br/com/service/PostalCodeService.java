package br.com.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.New;
import javax.inject.Inject;

import br.com.dao.address.AddressDao;
import br.com.entity.AddresEntity;
import br.com.rest.to.MessageResponse;
import br.com.service.exception.PostalCodeException;
import br.com.util.Constantes;
import br.com.util.messages.MessageUtil;

@Stateless
public class PostalCodeService {
	
	@Inject
	@New
	private AddressDao addressDao;
	
	public PostalCodeService() {
		super();
	}

	public PostalCodeService(AddressDao addressDao) {
		this.addressDao = addressDao;
	}
	
	/**
	 * Metodo responsavel por recuperar um endereço por cep
	 * 
	 * @param postalCode
	 * @return
	 * @throws PostalCodeException
	 */
	public AddresEntity search(String postalCode) throws PostalCodeException {
		validPostalCode(postalCode);

		AddresEntity cepEntity = addressDao.searchForPostalCode(postalCode);

		if (cepEntity == null) {
			for (int i = 1; i <= 3; i++) {
				postalCode = postalCode.substring(0, postalCode.length() - (i));
				for (int j = 1; j <= i; j++) {
					postalCode += "0";
				}
				cepEntity = addressDao.searchForPostalCode(postalCode);
				if (cepEntity != null) {
					break;
				}
			}
		}

		return cepEntity;
	}

	public void validPostalCode(String postalCode) throws PostalCodeException {
		isValid(postalCode);
	}

	private static void isValid(String cep) throws PostalCodeException {
		boolean valide = true;
		if (cep == null || cep.length() == 0) {
			List<MessageResponse> erros = new ArrayList<>();
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_CEP_INVALIDO)));			
			throw new PostalCodeException(erros);
		}

		cep = cep.replace(".", "");
		cep = cep.replace("-", "");

		if (cep.length() < 8) {
			valide = false;
		}

		try {
			int cepInt = Integer.parseInt(cep);
			if (cepInt < 1000000) {
				valide = false;
			}
		} catch (Exception e) {
			valide = false;
		}

		if (!valide) {
			List<MessageResponse> erros = new ArrayList<>();
			erros.add(new MessageResponse(MessageUtil.key(Constantes.MSG_CEP_INVALIDO)));			
			throw new PostalCodeException(erros);
		}
	}

}
