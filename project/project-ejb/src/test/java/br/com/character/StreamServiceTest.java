package br.com.character;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import br.com.caracter.Stream;
import br.com.caracter.StreamImpl;
import br.com.caracter.StreamService;

public class StreamServiceTest {
	
	@Test
	public void testStreamComCharUnico() {
		String semCharUnico = "AaaBBbbCCcc";
		
		Stream stream = new StreamImpl(semCharUnico);
		
		try {
			char retorno = StreamService.firstChar(stream);
			
			assertTrue(retorno == 'A');
		} catch (IOException e) {
			assertTrue(false);
		} catch (Throwable e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testStreamComCharVariosCharsUnicos() {
		String semCharUnico = "AABCc";
		
		Stream stream = new StreamImpl(semCharUnico);
		
		try {
			char retorno = StreamService.firstChar(stream);
			
			assertTrue(retorno == 'B');
		} catch (IOException e) {
			assertTrue(false);
		} catch (Throwable e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testStreamComConexaoFechada() {
		String semCharUnico = "AABCc";
		
		Stream stream = new StreamImpl(semCharUnico);
		
		try {
			StreamService.firstChar(stream);
			
			stream.getNext();
			
		} catch (IOException e) {
			assertTrue(true);
		} catch (Throwable e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testStreamSemCharUnico() {
		String semCharUnico = "AAaaBBbbCCcc";
		
		Stream stream = new StreamImpl(semCharUnico);
		
		try {
			StreamService.firstChar(stream);
			assertTrue(false);
		} catch (IOException e) {
			assertTrue(false);
		} catch (Throwable e) {
			assertTrue(true);
		}
	}

}
