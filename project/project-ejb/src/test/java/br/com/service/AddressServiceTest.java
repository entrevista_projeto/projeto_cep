package br.com.service;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.dao.address.AddressDao;
import br.com.entity.AddresEntity;
import br.com.service.AddressService;
import br.com.service.PostalCodeService;
import br.com.service.exception.AddressException;

public class AddressServiceTest {

	private AddressService addressService;

	@Mock
	private AddressDao dao;

	@Mock
	private PostalCodeService postalCodeService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		addressService = new AddressService(dao, postalCodeService);
	}

	@Test
	public void testSaveAddress() throws AddressException {

		AddresEntity address = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");

		AddresEntity addressSave = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP", "B Assunção",
				"AP 23");
		addressSave.setId(1);

		when(dao.save(address)).thenReturn(addressSave);

		AddresEntity addressRetorno = addressService.save(address);
		Assert.assertEquals(addressSave.getId(), addressRetorno.getId());

	}

	@Test
	public void testUpdateAddress() throws AddressException {
		AddresEntity addressUpdate = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP",
				"B Assunção", "AP 23");
		addressUpdate.setId(1);

		when(dao.update(addressUpdate)).thenReturn(addressUpdate);

		AddresEntity addressRetorno = addressService.update(addressUpdate);
		Assert.assertEquals(addressUpdate.getId(), addressRetorno.getId());
	}

	@Test
	public void testSearchAddress() throws AddressException {
		AddresEntity addressSearch = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP",
				"B Assunção", "AP 23");
		addressSearch.setId(1);

		when(dao.searchById(1)).thenReturn(addressSearch);

		AddresEntity addressRetorno = addressService.searchById(1);
		Assert.assertEquals(addressSearch.getId(), addressRetorno.getId());
	}
	
	@Test
	public void testRemoveAddres() throws AddressException {
		AddresEntity addressSearch = new AddresEntity("rua Vicente", "1000", "São Paulo", "09896200", "SP",
				"B Assunção", "AP 23");
		addressSearch.setId(1);

		when(dao.remove(1)).thenReturn(addressSearch);

		AddresEntity addressRetorno = addressService.remove(1);
		Assert.assertEquals(addressSearch.getId(), addressRetorno.getId());
	}

}
