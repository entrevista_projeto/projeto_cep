package br.com.service;
import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.entity.AddresEntity;
import br.com.service.PostalCodeService;
import br.com.service.exception.PostalCodeException;

public class PostalCodeServiceTest {

	private static PostalCodeService postalCodeService;

	@BeforeClass
	public static void init() {
		postalCodeService = new PostalCodeService();
	}

	@Test
	public void postalCodeInvalid() {
		String postalCoDeInvalid = "1234567892365889";
		try {
			postalCodeService.search(postalCoDeInvalid);
			assertTrue(false);
		} catch (PostalCodeException e) {
			assertTrue(true);
		}
	}

	@Test
	public void postalCodeNull() {
		String postalCoDeInvalid = null;
		try {
			postalCodeService.search(postalCoDeInvalid);
			assertTrue(false);
		} catch (PostalCodeException e) {
			assertTrue(true);
		}
	}

	@Test
	public void postalCodeEmpty() {
		String postalCoDeInvalid = "";
		try {
			postalCodeService.search(postalCoDeInvalid);
			assertTrue(false);
		} catch (PostalCodeException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void postalCodeValid() {
		String postalCoDeInvalid = "09861690";
		try {
			postalCodeService.search(postalCoDeInvalid);
			assertTrue(true);
		} catch (PostalCodeException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void postalCodeReturnAddress() {
		String postalCoDeInvalid = "09861690";
		try {
			AddresEntity postal = postalCodeService.search(postalCoDeInvalid);
			assertTrue(postal != null);
		} catch (PostalCodeException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void postalCodeReturnAddressCharacter() {
		String postalCoDeInvalid = "09861-690";
		try {
			AddresEntity postal = postalCodeService.search(postalCoDeInvalid);
			assertTrue(postal != null);
		} catch (PostalCodeException e) {
			assertTrue(false);
		}
	}

}
